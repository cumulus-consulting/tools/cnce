#!/bin/bash

PROGRAM="Cumulus NetQ Chaos Engine"

echo "##########################################"
echo "### Installing $PROGRAM... ###"
echo "##########################################"

if [ "$(whoami)" != "root" ]; then
	echo -e "\nERROR: Root is required to run this script! Please re-run with:\n"
	echo -e "            'sudo -H ./install.sh'\n"
	exit 1
fi

set -e

echo "  - Creating /etc/cnce/ directory ..."
mkdir -p /etc/cnce/

echo "  - Copying cnce.py application ..."
cp ./cnce.py /etc/cnce/cnce.py
cp ./run_command.yml /etc/cnce/run_command.yml

echo "  - Creating systemd unit file ..."
cat << EOT > /etc/systemd/system/cnce.service
[Unit]
Description=Cumulus NetQ Chaos Engine

[Service]
User=cumulus
Restart=on-failure
StartLimitInterval=1m
StartLimitBurst=3
TimeoutStartSec=0
ExecStart=/etc/cnce/cnce.py

[Install]
WantedBy=multi-user.target
EOT

echo "  - Creating systemd timer file ..."
cat << EOT > /etc/systemd/system/cnce.timer
[Timer]
OnBootSec=3min
EOT

echo "  -Enabling $PROGRAM to start automatically at boot..."
systemctl daemon-reload
systemctl enable cnce.timer
systemctl start cnce.service
echo "##############################"
echo "### Installation Complete! ###"
echo -e "##############################\n"
echo "  1). Start the service."
echo "        'sudo systemctl start cnce.service'"
echo "  2). Check the health of the running service."
echo "        'sudo systemctl status cnce.service'"
echo "  3). Monitor syslog or systemd journal for updates from the service."
echo "        'sudo grep cnce /var/log/syslog' or"
echo -e "        'sudo journalctl -u cnce -f'\n"

