import json, logging, requests
from pprint import pformat

class CumulusNetQ():
    def __init__(self, baseurl):
        self.logger = logging.getLogger()
        self.baseurl = baseurl
        self.logger.debug(self.baseurl)
        self.headers = {"accept":"application/json", "Content-Type":"application/json"}

    def login(self, username, password):
        status = False
        url = self.baseurl + '/netq/auth/v1/login'
        data = {"username":username, "password":password}
        response = requests.post(url, json=data, headers=self.headers).json()
        self.logger.debug(pformat(response))
        if 'premises' in response.keys():
            self.headers['Authorization'] = response['access_token']
            status = True
        return status

    def make_admin(self, userid):
        url = self.baseurl + '/netq/user/v1/'
        data = {
            'id': userid,
            'role': 'admin'
        }
        response = requests.put(url, json=data, headers=self.headers).json()
        self.logger.debug(pformat(response))
        return response

    def create_demo_user(self, email, custid, adminpw):
        url = self.baseurl + '/netq/user/v1/'
        data = {
            'id': email.split('@')[0] + '+netq-demo@' + email.split('@')[1],
            'cust_id': custid,
            'first_name': email.split('@')[0],
            'last_name': '',
            'email': email,
            'is_ldap_user': False,
            'role': 'user',
            'admin_password': adminpw,
            'password': 'netqdemo',
            'default_workbench':{'workspace_id':'DEFAULT', 'workbench_id':'DEFAULT'},
            'preferences': {}
        }
        response = requests.post(url, json=data, headers=self.headers).json()
        self.logger.debug(pformat(response))
        return response

    def delete_demo_user(self, email):
        url = self.baseurl + '/netq/user/v1/' + email.split('@')[0] + '+netq-demo@' + email.split('@')[1]
        response = requests.delete(url, headers=self.headers).json()
        self.logger.debug(response)
        return response

    def get_workbenches(self):
        url = self.baseurl + '/netq/user/v1/workbench'
        response = requests.get(url, headers=self.headers).json()
        self.logger.debug(pformat(response))
        return response
    
    def get_workbench_cards(self, wbid, workspace):
        url = self.baseurl + '/netq/user/v1/workbench/workspace/' + workspace + '/workbench/' + wbid
        response = requests.get(url, headers=self.headers).json()
        self.logger.debug(pformat(response))
        return response

    def populate_workbenches(self, workbenches, cards):
        for workbench in workbenches:
            # create the workbench
            url = self.baseurl + '/netq/user/v1/workbench'
            if workbench['workbench_id'] != "DEFAULT":
                data = [{
                    'data': {'name': workbench['workbench_name']},
                    'is_public': False,
                    'workbench_name': workbench['workbench_name'],
                    'workspace_id': 'DEFAULT'
                }]
                response = requests.post(url, json=data, headers=self.headers).json()
                self.logger.debug(pformat(response))
                # populate the cards
                url = self.baseurl + '/netq/user/v1/workbench/workspace/DEFAULT/workbench/' + response['workbench_ids'][0]
                data = cards[workbench['workbench_id']]
                response = requests.post(url, json=data, headers=self.headers)
                self.logger.debug(response)
