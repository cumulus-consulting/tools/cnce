#!/usr/bin/env python3

"""Usage: netq-demo.py [options] (--create-demo-user <email> | --delete-demo-user <email> | --make-admin <userid>)

Options:
  -c CONFIG, --config CONFIG    Configuration file [default: config.yml]
  -h, --help
  -l LEVEL, --log-level LEVEL   Logging level (CRITICAL, ERROR, WARN, INFO, DEBUG). [default: INFO]
"""

import logging, yaml
from CumulusNetQ import CumulusNetQ
from docopt import docopt
from pprint import pformat

def main(args):
    with open(args['--config']) as configfile:
        cfg = yaml.load(configfile, Loader=yaml.Loader)
    netq = CumulusNetQ(cfg['url'])
    if args['--create-demo-user']:
        if netq.login(cfg['adminUsername'], cfg['adminPassword']):
            workbenches = netq.get_workbenches()
            cards = {}
            for workbench in workbenches:
                if workbench['workbench_id'] != "DEFAULT":
                    cards[workbench['workbench_id']] = netq.get_workbench_cards(workbench['workbench_id'], workbench['workspace_id'])
                    netq.create_demo_user(args['<email>'], cfg['adminAccountId'], cfg['adminPassword'])
                    netq_demouser = CumulusNetQ('https://api.air.netqdev.cumulusnetworks.com')
                    if netq_demouser.login(args['<email>'].split('@')[0] + '+netq-demo@' + args['<email>'].split('@')[1], 'netqdemo'):
                        netq_demouser.populate_workbenches(workbenches, cards)
                    else:
                        print("API authentication failed.")
                        exit(1)
        else:
            print("API authentication failed.")
            exit(1)
    elif args['--delete-demo-user']:
        if netq.login(cfg['adminUsername'], cfg['adminPassword']):
            netq.delete_demo_user(args['<email>'])
        else:
            print("API authentication failed.")
            exit(1)
    elif args['--make-admin']:
        if netq.login(cfg['airAdminUsername'], cfg['airAdminPassword']):
            netq.make_admin(args['<userid>'])
        else:
            print("API authentication failed.")
            exit(1)
    else:
        logger.error("Invalid options")

    #     netq.create_demo_user(args.userid, args.password, args.firstname, args.lastname, args.email, 90230, 'test')
    #     netq_demouser = CumulusNetQ('https://api.air.netqdev.cumulusnetworks.com')
    #     if not netq_demouser.login(args.userid, args.password):
    #         print("Authentication to API failed for demo user.")
    #         exit(1)
    #     netq_demouser.populate_workbenches(workbenches, cards)

if __name__ == "__main__":
    logging.basicConfig(level="ERROR", format='%(asctime)s %(filename)s:%(funcName)s - [%(levelname)s] %(message)s', datefmt='%Y/%m/%d %H:%M:%S')
    logger = logging.getLogger()
    arguments = docopt(__doc__)
    logger.setLevel(arguments['--log-level'])
    logger.debug(arguments)
    main(arguments)
    exit(0)
