#!/usr/bin/env python3

import json, requests

baseurl = "https://api.air.netqdev.cumulusnetworks.com"
headers = {"accept":"application/json", "Content-Type":"application/json"}

# login and get access token
url = baseurl + '/netq/auth/v1/login'
data = {"username":"nmitchell+netq-demo@cumulusnetworks.com", "password":"test"}
response = requests.post(url, json=data, headers=headers)
print(response.json())
headers['Authorization'] = response.json()['access_token']

# get workbench data
url = baseurl + '/netq/user/v1/workbench'
response = requests.get(url, json=data, headers=headers)
print(response.json())

# create demo user
##  cust_id needs to reflect ID of netq-demo user
##  admin_passord is password of the netq-demo user
url = baseurl + '/netq/user/v1/'
data = {"id":"user1+netq-demo@cumulusnetworks.com", "cust_id":90230, "first_name":"NetqDemo", "last_name":"Testuser", "email":"nick@cumulusnetworks.com", "is_ldap_user":False, "role":"user", "admin_password":"test", "password":"nicktest", "default_workbench":{"workspace_id":"DEFAULT", "workbench_id":"DEFAULT"}, "preferences":{}}
response = requests.post(url, json=data, headers=headers)
print(response.json())

# login as the new user
url = baseurl + '/netq/auth/v1/login'
data = {"username":"user1+netq-demo@cumulusnetworks.com", "password":"nicktest"}
response = requests.post(url, json=data, headers=headers)
print(response.json())
headers['Authorization'] = response.json()['access_token']
