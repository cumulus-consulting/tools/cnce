#!/usr/bin/env python3

import json, requests

baseurl = "https://api.air.netqdev.cumulusnetworks.com"
headers = {"accept":"application/json", "Content-Type":"application/json"}

# login and get access token
url = baseurl + '/netq/auth/v1/login'
data = {"username":"admin", "password":"admin"}
response = requests.post(url, json=data, headers=headers)
print(response.json())
headers['Authorization'] = response.json()['access_token']

# change netq-demo@cumulusnetworks.com user role to admin
url = baseurl + '/netq/user/v1/'
data = {"id":"nmitchell+netq-demo@cumulusnetworks.com", "role":"admin"}
response = requests.put(url, json=data, headers=headers)
print(response.json())
