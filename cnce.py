#!/usr/bin/env python3

import argparse, logging, random, subprocess, time

event_list = [
                {"target":"leaf01", "failcmd":"add bond peerlink link down", "recovercmd":"del bond peerlink link down"},
                {"target":"leaf04", "failcmd":"add int swp52 link down", "recovercmd":"del int swp52 link down"},
                {"target":"border02", "failcmd":"add int swp53 link down", "recovercmd":"del int swp53 link down"},
                {"target":"border01", "failcmd":"add vxlan vniRED link down", "recovercmd":"del vxlan vniRED link down"},
                {"target":"leaf02", "failcmd":"add vxlan vni20 link down", "recovercmd":"del vxlan vni20 link down"}
             ]
time_between_events = {"min":90, "max":180}
event_length = {"min":30, "max":60}

def main():
    event = random.choice(event_list)
    subprocess.run(["ansible-playbook", "/etc/cnce/run_command.yml", "-e", "command='{}'".format(event['failcmd']), "-l", "{}".format(event['target'])], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
#    subprocess.run(["ansible-playbook", "/etc/cnce/run_command.yml", "-e", "command='{}'".format(event['failcmd']), "-l", "{}".format(event['target'])])
    logger.info("Triggering failure on {} -- net {}".format(event['target'], event['failcmd']))
    time.sleep(random.randint(event_length['min'], event_length['max']))
    subprocess.run(["ansible-playbook", "/etc/cnce/run_command.yml", "-e", "command='{}'".format(event['recovercmd']), "-l", "{}".format(event['target'])], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
#    subprocess.run(["ansible-playbook", "/etc/cnce/run_command.yml", "-e", "command='{}'".format(event['recovercmd']), "-l", "{}".format(event['target'])])
    logger.info("Triggering recovery on {} -- net {}".format(event['target'], event['recovercmd']))
    time.sleep(random.randint(time_between_events['min'], time_between_events['max']))

if __name__ == "__main__":
    # Configure logging
    logging.basicConfig(level="INFO", format='[%(levelname)s] %(message)s')
    logger = logging.getLogger()
    
    while True:
        main()

    exit(0)
